const express = require('express');
const router = express.Router();
const nvt = require('node-virustotal');


router.get('/:term', (req, res) => {
    const defaultTimedInstance = nvt.makeAPI();
    defaultTimedInstance.setKey('5e4c6f2ae264c8b37548843a2df638f47ae4ae7dd240df8e449f1fd2d025ce57');
    const theSameObject = defaultTimedInstance.fileLookup(req.params.term, function(err, res2){
        if (err) {
            console.log('error accured while cummunicating with virus total: ');
            console.log(err);
            res.status(200).json("0 0");
            return;
        }

        res2 = JSON.parse(res2);
        
        const malicious = res2["data"]["attributes"]["last_analysis_stats"]["malicious"];

        const undetected = res2["data"]["attributes"]["last_analysis_stats"]["undetected"];

        var result = "" + malicious + " " + undetected;
        res.status(200).json(result);
        return;


        });
});




module.exports = router;
