const express = require('express');
const router = express.Router();

//Attack model

const Attack = require('../../models/Attack');

//get request returns all attacks
router.get('/', (req, res) => {
    Attack.find()
    .then(attacks => res.json(attacks))
});



module.exports = router;