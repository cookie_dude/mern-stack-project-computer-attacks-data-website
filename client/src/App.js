import {useState, useEffect} from 'react';
import Header from './components/Header'
import Attacks from './components/Attacks'
import MyChatBot from './components/MyChatBot';
import MyParticles from './components/MyParticles';


function App() {

  const [attacks, setAttacks] = useState([]) 
  const [particlesOn, setParticlesOn] = useState(true) 
  const [searchTerm, setSearchTerm] = useState('')


  useEffect(() => {
    const getAttacks = async () => {
      const attacksFromServer = await fetchAttacks()
      setAttacks(attacksFromServer)
    }
    getAttacks()
  }, [])

  const fetchAttacks = async () => {
    const res = await fetch('http://localhost:5000/api/attacks')
    const data = await res.json()
    return data
  }


  return (
    <div style={{
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%"
    }}>
      <div style = {{backgroundColor: "#212121", height: "40px"}}>
      <button className = "btn" style={{right: "0px", position: 'absolute'}} onClick={() => setParticlesOn(!particlesOn)}>{particlesOn ? "Particles: ON" : "Particles: OFF"}</button>
      </div>
      {particlesOn&&<MyParticles className="myParticles"/>}
      
      <div className="container" style={{
            position: "absolute",
          }}>
        <dir className="bar">
          <Header />
          <input type="text" placeholder="Search..." onChange={event => {setSearchTerm(event.target.value)}} />
        </dir>
        <Attacks attacks = {attacks} searchTerm = {searchTerm}/>
        <MyChatBot setSearchTerm = {setSearchTerm}/>
      </div>
    </div>
  );
}



export default App;
