import {useState, useEffect} from 'react';


const VirusTotalSearch = ({steps}) => {
    const searchHash = steps.input.value.substr(steps.input.value.lastIndexOf(" ") + 1);
    const [dataOnFile, setDataOnFile] = useState([]) 
    
    useEffect(() => {
        const getData = async () => {
            const dataaa = await fetchData()
            setDataOnFile(dataaa)
        }
        getData()
    }, [])
    
    const fetchData = async () => {
        const res = await fetch(`http://localhost:5000/api/virustotal/${searchHash}`);
        const data = await res.json()
        return data
    }
    
    return (
        <div className="aaa">
            <p style={{color: dataOnFile.toString().split(" ")[0] === "0" ? "green" : "red" }}>{dataOnFile.length > 0 ? dataOnFile.toString().split(" ")[0]+"/"+(Number(dataOnFile.toString().split(" ")[0])+ Number(dataOnFile.toString().split(" ")[1]))+" marked this file as malicious" : "loading..."}</p>
        </div>
    )
}

export default VirusTotalSearch
