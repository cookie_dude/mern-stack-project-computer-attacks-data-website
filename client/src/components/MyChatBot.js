import ChatBot from 'react-simple-chatbot';
import { ThemeProvider } from "styled-components";

import VirusTotalSearch from './VirusTotalSearch';
import Cuckoo from './Cuckoo';

const theme = {
         background: "#313131",
         fontFamily: "Arial",
         headerBgColor: "rgb(62, 51, 117)",
         headerFontColor: "#fff",
         headerFontSize: "25px",
         botBubbleColor: "rgb(62, 51, 117)",
         botFontColor: "#fff",
         userBubbleColor: "#fff",
         userFontColor: "#4c4c4c"
        };

const MyChatBot = ({setSearchTerm}) => {
    return (
        <div className='chatBot'>
            <ThemeProvider theme={theme}>
                <ChatBot 
                    steps={[
                        {
                            id: '1',
                            message: 'enter command (enter "help" for list of available commands)',
                            trigger: 'input',
                        },
                        {
                            id: 'input',
                            user: true,
                            trigger: (previousValue) => {
                                switch (previousValue.value.split(" ")[0]) {
                                    case 'help':
                                        return 'help';
                                    case 'search':
                                        return 'search';
                                    case 'check':
                                        return 'check';
                                    case 'cuckoo':
                                        return 'cuckoo';
                                    default:
                                        return 'invalid';
                                
                                }
                            },
                        },
                        {
                            id: 'invalid',
                            message: 'Invalid command',
                            trigger: '1',
                        },
                        {
                            id: 'help',
                            message: 'Available commands:',
                            trigger: 'search help',
                        },
                        {
                            id: 'search help',
                            message: 'search - searches dattack descriptions for key phrases',
                            trigger: 'check help',
                        },
                        {
                            id: 'check help',
                            message: 'check - checks virustotal for hash code',
                            trigger: 'cuckoo help',
                        },
                        {
                            id: 'cuckoo help',
                            message: 'cuckoo - cuckoo sandbox commands, enter cuckoo -help for available cuckoo commands',
                            trigger: '1',
                        },
                        {
                            id: 'search',
                            message: (steps) => {
                                setSearchTerm(steps.steps.input.value.substr(steps.steps.input.value.indexOf(" ") + 1))
                            },
                            trigger: '1',
                        },
                        {
                            id: 'check',
                            component: <VirusTotalSearch/>,
                            trigger: '1',
                        },
                        {
                            id: 'cuckoo',
                            component: <Cuckoo/>,
                            trigger: '1',
                        },
                    ]}
                    floating={true}
                />
            </ThemeProvider>
        </div>
    )
}

export default MyChatBot