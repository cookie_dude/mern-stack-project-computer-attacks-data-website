import ReactReadMoreReadLess from "react-read-more-less";

const Attack = ({attack}) => {
    return (
        <div className='attack'>
            <h3>{attack.objects[0].name}</h3>
            <div className='attackDetail'>
                <ReactReadMoreReadLess
                    charLimit={200}
                    readMoreText={"Read more ▼"}
                    readLessText={"Read less ▲"}
                >
                    {attack.objects[0].description === undefined ? 'NO DESCRIPTION AVAILABLE' : attack.objects[0].description}
                </ReactReadMoreReadLess>
                <p>ID: {attack.objects[0].id}</p>
                <p>Platforms: {attack.objects[0].x_mitre_platforms === undefined ? 'NA' : attack.objects[0].x_mitre_platforms.toString()}</p>
                <ReactReadMoreReadLess
                    charLimit={200}
                    readMoreText={"Read more ▼"}
                    readLessText={"Read less ▲"}
                >
                    {"Detection: " + (attack.objects[0].x_mitre_detection === undefined ? 'NA' : attack.objects[0].x_mitre_detection)}
                </ReactReadMoreReadLess>
                <p>Phase: {attack.objects[0].kill_chain_phases === undefined ? "NA" : attack.objects[0].kill_chain_phases[0].phase_name}</p>
            </div>
        </div>
    )
}

export default Attack

