import Attack from './Attack'
import { Grid } from '@material-ui/core';

const Attacks = ( {attacks, searchTerm}) => {
  return (
        <Grid container spacing={3}>
          {attacks.length === 0 ? <h2 style={{margin: '80px auto'}}>loading attacks...</h2> :
            attacks.filter((val) => {
              return val.objects[0].description !== undefined && val.objects[0].description.toLowerCase().includes(searchTerm.toLowerCase())
            }).map((attack) => (
              <Grid key={attack._id} item xs={12} sm={4}>
                <Attack attack={attack} style={{display: "block"}}/>
              </Grid>
            ))}
        </Grid>
    )
}

export default Attacks
