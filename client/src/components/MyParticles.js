import Particles from 'react-particles-js';

const MyParticles = ( {attacks, searchTerm}) => {
  return (
    

	<div 
          style={{
            position: "absolute",
            top: 50,
            left: 0,
            width: "100%",
            height: "100%"
        	}}>
         <Particles
			params={{
				"particles": {
					"number": {
						"value": 100
					},
					"size": {
						"value": 4
					}
				},
				"interactivity": {
					"events": {
						"onhover": {
							"enable": false,
							"mode": "repulse"
						}
					}
				}
			}} />
      </div>

    )
}

export default MyParticles
