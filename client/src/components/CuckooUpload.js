import React, {useState} from 'react';

const server = "http://192.168.1.53:8090";


function CuckooUpload(){
	const [selectedFile, setSelectedFile] = useState();
	const [isFilePicked, setIsFilePicked] = useState(false);
    const [analysisResult, setAnalysisResult] = useState(false);

	const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
		setIsFilePicked(true);
	};

	const handleSubmission = async () => {
        var formData = new FormData();

		formData.append('file', selectedFile);

		const response = await fetch(server + "/tasks/create/file", {
            method: "POST",
            body: formData
        })

		

        const data = await response.json()
		console.log(data)
        
        var response2

        const sleep = ms => new Promise(res => setTimeout(res, ms))
        do{
            response2 = await fetch(server + "/tasks/summary/" + data["task_id"].toString(), {
                method: "GET",
				body: formData
            })
            sleep(5000)
        }while(response2.status !== 404);
		setAnalysisResult(response.json())

	};

	return(
        <div>
			{((analysisResult === false)&&<input type="file" name="file" onChange={changeHandler} /> )}
			{isFilePicked ? (
				<div>
					<p>Filename: {selectedFile.name}</p>
					<p>Filetype: {selectedFile.type}</p>
					<p>Size in bytes: {selectedFile.size}</p>
					<p>
						lastModifiedDate:{' '}
						{selectedFile.lastModifiedDate.toLocaleDateString()}
					</p>
				</div>
			) : (
				<p>Select a file to show details</p>
			)}
                {((analysisResult === false) ? <button className="btn" onClick={handleSubmission}>Submit</button> : 
				<p>cuckoo score: {analysisResult["info"]["score"]}/10</p>)}
		</div>
	)
}


export default CuckooUpload