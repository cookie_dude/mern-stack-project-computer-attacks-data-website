import {useState} from 'react';

import CuckooUpload from './CuckooUpload';

const Cuckoo = ({steps}) => {
    const [command, setCommand] = useState(
        steps.input.value.substr(steps.input.value.indexOf(" ") + 1));
    var content;
    switch (command) {
        case '-help':
            content = <p>cuckoo <br/>
            -upload : upload a file<br/>
            aa</p>;    
            break;
        case '-upload':
            content = <CuckooUpload />;
            break;
        default:
            content = <p>invalid cuckoo command: {command}</p>;
            break;
    }
    return <div className = "cuckoo">{content}</div>;
}

export default Cuckoo