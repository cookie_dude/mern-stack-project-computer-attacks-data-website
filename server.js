const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const attacks = require('./routes/api/attacks');
const virusTotal = require('./routes/api/virusTotal');

const app = express();

app.use(cors({
    origin: '*'
})); //cors config

app.use(bodyParser.json());

//DB URI
const db = "mongodb+srv://edo:4044@cluster0.vgnxi.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

//connect
mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err));


app.use('/api/attacks', attacks);
app.use('/api/virusTotal', virusTotal);

const port = 5000

app.listen(port, () => console.log(`server listening on port ${port}`));

//aa